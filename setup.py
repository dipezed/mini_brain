from setuptools import setup, find_packages

setup(
    name='dimitri',
    version='0.1',
    package_dir={'': 'src'},  # Specify that packages are under src
    packages=find_packages(where='src'),  # Find packages in src
    install_requires=[
        # your dependencies here, e.g.,
        # 'numpy',
        # 'pandas',
    ],
)

