import numpy as np

V_REST = 0 # mv | can be -65 if desired
DT = 0.01 # ms
TOTAL_TIME = 150 # ms
C = 1 # uF/cm^2

E_NA = 115 + V_REST # mv
E_K = -12 + V_REST # mv
E_LEAK = 10.6 + V_REST # mv # Normaly it is 10.6 + V_rest

G_NA = 120 # ms/cm^2
G_K = 36 # ms/cm^2
G_LEAK = 0.3# ms/cm^2

RATIO = 1/4 # one inhibitory on 4 neurons

TIME_PER_VALUE = int(100 / DT) #ms

# for hod-hux; the time to reset the list(prevent memory overflow)
TIME_CIRCULAR = 10002

# --------------- TRAINING ---------------------
INPUT_SIZE = 100
MIDDLE_SIZE = 10 

BATCH_SIZE = 1  #augmente la complexite
THRESHOLD = V_REST + 60 # when the hh model generated his wave
#then a spike is send when the threshold is reach all time long
# ----weight max_min-----
W_MAX = 2
W_MIN = 0
#-----weight init--------
W_LOW = 0 
W_HIGH = 1.5

# --------------- ENCODING ----------------------
# For Encoding value
VALUE_SPIKE = 1 # spike value in mv 
MAX_RATE = 0.01 * 1000 / INPUT_SIZE #Rate for the input spike to appear 
GAP_SPIKE = int(1 / DT) # 0.5 -> 20 mv biologicaly
GAP_REST = 1 # the number of 0 after a spike


